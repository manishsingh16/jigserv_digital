import React from 'react';
import './style/api.css';
import "./style/Fourth_section.css"
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

const Mapp=(props)=>{
    const mapStyles = {
        
          width: '50%',
          height: '77%'
        
      };

    return(
        
         <Map
          google={props.google}
          zoom={16}
          style={mapStyles}
          initialCenter={{ lat: 19.059489826419718, lng: 72.83252715846106}}
        className="mapp" >
          <Marker position={{ lat: 19.059489826419718, lng: 72.83252715846106}} style={mapStyles}/>
        </Map>
        
    )
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyDC6vCKD9OmUiEbxwzzsiD3C4BUPeMW_Z0")
  })(Mapp)

