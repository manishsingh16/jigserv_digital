import React from 'react';
import Header from './header';
import  MiddleDiv from './middleDiv';
import Footer from './footer';
import Fourth_section from './Fourth_section';
import './style/landingPage.css'
function LandingPage(){
    return(
        <>
        <Header />
        <MiddleDiv />
        <Fourth_section />
        <Footer />
        </>
    )
}
export default LandingPage;