import React from "react";
import footer_logo from "./footer-logo.svg";
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import YouTubeIcon from '@material-ui/icons/YouTube';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import "./style/footer.css";

function Footer() {
  return (
    <div className="footer">
      <div className="footer_first">
        <div className="first_column">
          <h2>Trending Programs</h2>
          <div className="features">
            <div className="features_one">
              <p>Executive PG Diploma Programme in Management...</p>
              <p>Digital Marketing</p>
              <p>Digital Mindset and Design Thinking</p>
              <p>Business Analytics: Decision-Making Using Data</p>
              <p>Artificial Intelligence and Machine Learning for...</p>
            </div>
            <div className="features_two">
              <p>Big Data Analytics (Online)</p>
              <p>Advanced Business Analytics Program</p>
              <p>Business Leadership Program for In-House Counsel</p>
              <p>Finance and Accounting for Businesses Managers</p>
            </div>
          </div>
        </div>
        <div className="second_column">
          <h2>Featured Programs</h2>
          <div className="features">
            <div className="features_one">
              <p>Executive PG Diploma Programme in Management...</p>
              <p>Digital Mindset and Design Thinking</p>
            </div>
            <div className="features_two">
              <p>Digital Marketing</p>
              <p>Marketing Analytics: Data Driven Marketing</p>
            </div>
          </div>
        </div>
        <div className="third_column">
          <h2>Program Categories</h2>
          <div className="features">
            <div className="features_one">
              <p>All Categories</p>
              <p>Digital Transformation</p>
              <p>Business Analytics</p>
              <p>Leadership/HRM</p>
              <p>Strategy</p>
              <p>General Management</p>
            </div>
            <div className="features_two">
              <p>Innovation</p>
              <p>Finance & FinTech</p>
              <p>Operations</p>
              <p>Marketing</p>
              <p>Technology</p>
            </div>
          </div>
        </div>
      </div>

      <div className="footer_second">
          
          <div className="footer_logo">
              <img src={footer_logo}/>
          </div>

          <div className="footer_home">
              <p><a href="#">Home</a></p>
              <p><a href="#">About</a></p>
              <p><a href="#">Contact Us</a></p>
              <p><a href="#">Privacy Policy</a></p>
              <p><a href="#">Terms and Conditions</a></p>
            
          </div>


          <div className="follow_us">
            <h4>FOLLOW US ON:</h4>
            
            <div className="socail_media_logo">
              <FacebookIcon/><TwitterIcon/><InstagramIcon/>< LinkedInIcon/><YouTubeIcon/><WhatsAppIcon/>
            </div>
            <h4>CONTACT  US</h4>
            <div className="phone_no"><PhoneIcon /> <p> +91 22 26550659</p></div>
            <div className="phone_no"><EmailIcon /> <p> contact.india@jigserv.com</p></div>
           
          </div>
      </div>
    </div>
  );
}

export default Footer;
