import React from "react";
import Map from "./map";
import List from "./list";
import "./style/Fourth_section.css";

const Fourth_section = () => {
  return (
    <div className="map">
      <div className="map_section">
        <Map />
      </div>

      <div className="categories">
        <h1>Explore Programs by Category</h1>
        <p>Explore Executive Education Programs to match your learning needs</p>
        <List />
      </div>
    </div>
  );
};
export default Fourth_section;
