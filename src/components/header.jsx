import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import ForumIcon from '@material-ui/icons/Forum';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import AddToPhotosSharpIcon from '@material-ui/icons/AddToPhotosSharp';
import BallotSharpIcon from '@material-ui/icons/BallotSharp';
import AddCommentSharpIcon from '@material-ui/icons/AddCommentSharp';
import './style/header.css'
import image from './download.svg';

function Header(){
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

return(
    <>
    <div className="header">
        <div className='img'><img src={image}/></div>

        <div className='header_features'>
        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
  Categories <ArrowDropDownIcon />
</Button>
<Menu
  id="simple-menu"
  anchorEl={anchorEl}
  keepMounted
  open={Boolean(anchorEl)}
  onClose={handleClose}
>
  <MenuItem onClick={handleClose}>All Categories</MenuItem>
  <MenuItem onClick={handleClose}>Innovations</MenuItem>
  <MenuItem onClick={handleClose}>Digital Transformations</MenuItem>
  <MenuItem onClick={handleClose}>Finance and Fintech</MenuItem>
  <MenuItem onClick={handleClose}>Buisness Analytics</MenuItem>
  <MenuItem onClick={handleClose}>Operations</MenuItem>
  <MenuItem onClick={handleClose}>Leadership/HRM</MenuItem>

</Menu>
<Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
  Universities <ArrowDropDownIcon />
</Button>
<Menu
  id="simple-menu"
  anchorEl={anchorEl}
  keepMounted
  open={Boolean(anchorEl)}
  onClose={handleClose}
>
<MenuItem onClick={handleClose}>All Categories</MenuItem>
  <MenuItem onClick={handleClose}>Innovations</MenuItem>
  <MenuItem onClick={handleClose}>Digital Transformations</MenuItem>
  <MenuItem onClick={handleClose}>Finance and Fintech</MenuItem>
  <MenuItem onClick={handleClose}>Buisness Analytics</MenuItem>
  <MenuItem onClick={handleClose}>Operations</MenuItem>
  <MenuItem onClick={handleClose}>Leadership/HRM</MenuItem>
</Menu>
 <p> <a href="#">Course Recommendation </a><BallotSharpIcon/></p>
 <p> <a href="#">For Organisation </a><AddToPhotosSharpIcon/></p>
 <p> <a href="#">Discussion Forum </a> <ForumIcon/></p>
 <p> <a href="#">Blog</a> <AddCommentSharpIcon /></p>

        </div>
    </div>
    </>
)
};

export default Header;