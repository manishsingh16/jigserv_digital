import React from 'react';
import './style/middleDiv.css';
import image from "./bg2.jpg";
import Api from './Api';
import Card from './card';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";

function MiddleDiv(){

return(
    <>
    <div className="middleDiv">
        <div className="first_section">
            <img src={image}/> 
            
            <form className="form-wrapper cf">
  	<input type="text" placeholder="Search for Courses / Programs" required/>
	  <button type="submit">Search</button>
       </form> 
        </div>


        <div className="second_section">
        <AliceCarousel autoPlay infinite autoPlayInterval="900">
        <Api/>
        </AliceCarousel>
            
        </div>
    </div>
    </>
)
};

export default MiddleDiv;